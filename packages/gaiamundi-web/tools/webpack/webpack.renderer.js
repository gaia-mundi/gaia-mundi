const rules = require('./webpack.rules');
const plugins = require('./webpack.plugins');

module.exports = {
  target: 'electron-renderer',
  module: {
    rules,
  },
  plugins: plugins,
  resolve: {
    fallback: {
      path: require.resolve('path-browserify'),
      os: require.resolve('os-browserify/browser'),
    },
    extensions: ['.js', '.ts', '.jsx', '.tsx', '.css'],
    alias: {
      // Custom Aliases
      ...require('./webpack.aliases'),
    },
  },
  stats: 'minimal',
  /**
   * Fix: Enable inline-source-map to fix following:
   * Dev tools: unable to load source maps over custom protocol
   */
  devtool: 'inline-source-map',
};

/* eslint-disable @typescript-eslint/no-var-requires */

const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');
const webpack = require('webpack');

module.exports = {
  entry: {
    embed: './src/embed.tsx',
  },
  output: {
    path: __dirname + '/build/assets/',
    filename: '[name].bundle.js',
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.json'], // Resolve these extensions
    alias: {
      pages: path.resolve(__dirname, 'src/pages'),
      components: path.resolve(__dirname, 'src/components'),
      interfaces: path.resolve(__dirname, 'src/interfaces'),
      hooks: path.resolve(__dirname, 'src/hooks'),
      utils: path.resolve(__dirname, 'src/utils'),
      services: path.resolve(__dirname, 'src/services'),
      config: path.resolve(__dirname, 'src/config'),
      '@assets': path.resolve(__dirname, 'src/assets'),
      '@common': path.resolve(__dirname, 'src/common'),
      '@src': path.resolve(__dirname, 'src'),
    },
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: {
          loader: 'ts-loader',
          options: {
            configFile: 'tsconfig-embed.config.json', // Using the custom tsconfig file
          },
        },
        exclude: /node_modules/,
      },
      // CSS loader configuration
      {
        test: /\.css$/,
        use: [
          'style-loader', // This loader injects CSS into the DOM.
          'css-loader', // This loader resolves @import and url() like import/require() and will resolve them.
        ],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './public/index.html',
      filename: 'embed.html',
      chunks: ['embed'],
    }),
    new webpack.IgnorePlugin({
      checkResource(resource) {
        // Ignore electron app specfic folders
        return resource.includes('electron');
      },
    }),
  ],
  // Other configurations like module rules
};

const path = require('path');
const webpack = require('webpack');

module.exports = function override(config, env) {
  // do stuff with the webpack config...
  config.plugins = [
    ...config.plugins,
    new webpack.IgnorePlugin({
      checkResource(resource) {
        // Ignore electron app specfic folders
        return resource.includes('electron');
      },
    }),
  ];

  return config;
};

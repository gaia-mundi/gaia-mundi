import { Alert } from 'components/Alert/Alert';
import { ApiErrorAlert } from 'components/Alert/ApiErrorMessage';
import { LoadingMessage } from 'components/Loader/LoadingMessage';
import { ApiData, ApiDocument, ApiError } from 'interfaces/api';
import { DatasetColumn } from 'interfaces/column';
import { GeoMap } from 'interfaces/geo-map';
import { IndicatorStub } from 'interfaces/indicator';
import { PageCarto } from 'interfaces/page-carto';
import React, { useMemo } from 'react';
import { UseQueryResult, useQuery } from 'react-query';
import { useService } from './useService';
import { ContentType } from 'services/abstract';
import { SnapshotConfigs } from 'interfaces/snapshot';

type PageCartoContextValue = UseQueryResult<ApiDocument<PageCarto>, unknown> & {
  pageCartoId: number;
  pageCartoConfigs?: SnapshotConfigs;
  map?: GeoMap | null;
  columns: DatasetColumn[];
  indicators: ApiData<IndicatorStub>[];
};

const PageCartoContext = React.createContext<PageCartoContextValue | null>(
  null
);
PageCartoContext.displayName = 'PageCartoContext';

export interface PageCartoProviderProps {
  children: React.ReactNode;
  id: number;
}

export const PageCartoProvider = ({
  id,
  children,
}: PageCartoProviderProps): JSX.Element => {
  const pageCartoService = useService(ContentType.PAGE_CARTOS);
  const query = useQuery({
    queryKey: ['page-carto', id],
    queryFn: async () => {
      return await pageCartoService.getPageCartoById(id);
    },
    keepPreviousData: true,
    // The query will not execute until the userId exists
    enabled: !!id,
  });

  const pageCarto = query.data;

  const map = pageCarto?.data?.map;
  // Compute columns by merging data fragments
  const columns = useMemo(
    () =>
      (pageCarto?.data?.data_fragments || []).reduce((acc, curr) => {
        const cols = curr.columns
          .filter((column) => {
            return !column.isGeoCode;
          })
          .map((column) => {
            return {
              ...column,
              dataset: curr.dataset.name,
            };
          });
        return acc.concat(cols);
      }, [] as DatasetColumn[]),
    [pageCarto]
  );

  const indicators = useMemo(
    () => pageCarto?.data?.indicators || [],
    [pageCarto]
  );

  const pageCartoConfigs = useMemo(
    () => pageCarto?.data?.configs || undefined,
    [pageCarto]
  );

  if (query.isLoading) {
    return <LoadingMessage />;
  }

  if (query.isError) {
    return <ApiErrorAlert error={query.error as ApiError} />;
  }

  if (!query.data) {
    return <Alert type="info">Aucun contenu à afficher.</Alert>;
  }

  return (
    <PageCartoContext.Provider
      value={{
        pageCartoId: id,
        pageCartoConfigs,
        map,
        columns,
        indicators,
        ...query,
      }}
    >
      {children}
    </PageCartoContext.Provider>
  );
};

export const usePageCarto = () => {
  const context = React.useContext(PageCartoContext);
  if (!context) {
    throw new Error(`usePageCarto must be used within an PageCartoProvider`);
  }
  return context;
};

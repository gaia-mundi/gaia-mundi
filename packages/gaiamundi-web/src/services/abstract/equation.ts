import { ApiCollection } from 'interfaces/api';
import { Equation } from 'interfaces/equation';

export abstract class EquationService {
  abstract getEquations(): Promise<ApiCollection<Equation>>;
}

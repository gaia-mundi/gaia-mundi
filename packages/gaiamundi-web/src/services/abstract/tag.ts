import { ApiCollection, ApiDocument } from 'interfaces/api';
import { Tag } from 'interfaces/tag';

export abstract class TagService {
  abstract getAllTags(): Promise<ApiCollection<Tag>>;
  abstract getAllTagsByOwner(ownerId: number): Promise<ApiCollection<Tag>>;
  abstract createTag(data: Tag): Promise<ApiDocument<Tag>>;
}

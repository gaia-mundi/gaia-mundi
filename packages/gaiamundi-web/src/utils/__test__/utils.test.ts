import { getCharRange } from '../utils';

describe('getCharRange', () => {
  it('returns an array of characters within the specified range (inclusive)', () => {
    const start = 'A';
    const stop = 'C';

    const result = getCharRange(start, stop);

    expect(result).toEqual(['A', 'B', 'C']);
  });
});

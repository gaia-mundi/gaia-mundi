import { getUrlQueryParam } from '../url';

describe('getUrlQueryParam', () => {
  it('returns the value of the query parameter when it exists and is not an empty string', () => {
    const router = {
      query: {
        paramName: 'value',
      },
    };
    const paramName = 'paramName';

    const result = getUrlQueryParam(router, paramName);

    expect(result).toEqual('value');
  });

  it('returns null when the query parameter does not exist', () => {
    const router = {
      query: {},
    };
    const paramName = 'paramName';

    const result = getUrlQueryParam(router, paramName);

    expect(result).toEqual(null);
  });

  it('returns null when the query parameter exists but is an empty string', () => {
    const router = {
      query: {
        paramName: '',
      },
    };
    const paramName = 'paramName';

    const result = getUrlQueryParam(router, paramName);

    expect(result).toEqual(null);
  });

  it('returns the first value of the query parameter when it is an array', () => {
    const router = {
      query: {
        paramName: ['value1', 'value2'],
      },
    };
    const paramName = 'paramName';

    const result = getUrlQueryParam(router, paramName);

    expect(result).toEqual('value1');
  });

  it('returns null when the query parameter is an empty array', () => {
    const router = {
      query: {
        paramName: [],
      },
    };
    const paramName = 'paramName';

    const result = getUrlQueryParam(router, paramName);

    expect(result).toEqual(null);
  });
});

import { adjustColorComponent, generateNewColor } from 'utils/color';

describe('adjustColorComponent', () => {
  test('should adjust component within the range', () => {
    expect(adjustColorComponent(100, 50)).toBe(150);
  });

  test('should wrap component exceeding 255 to start', () => {
    expect(adjustColorComponent(230, 30)).toBe(5);
  });

  test('should wrap component going below 0 to end', () => {
    expect(adjustColorComponent(10, -20)).toBe(245);
  });

  test('should handle no adjustment', () => {
    expect(adjustColorComponent(123, 0)).toBe(123);
  });

  test('should handle negative adjustment within range', () => {
    expect(adjustColorComponent(100, -50)).toBe(50);
  });
});

// Helper function to mock Math.random with a sequence of values
function mockMathRandom(...values: number[]) {
  let index = 0;
  jest.spyOn(Math, 'random').mockImplementation(() => {
    const value = values[index];
    index = (index + 1) % values.length; // Cycle through the provided values
    return value;
  });
}

describe('generateNewColor', () => {
  afterEach(() => {
    jest.restoreAllMocks(); // Restore Math.random after each test
  });

  test('should adjust red component with positive adjustment', () => {
    mockMathRandom(0, 0.5); // Cycle 1, adjustment around 35
    expect(generateNewColor('#FF0000')).toBe('#FF00EB');
  });

  test('should adjust green component with negative adjustment', () => {
    mockMathRandom(0.5, 0.5); // Cycle 2, adjustment around -35
    expect(generateNewColor('#00FF00')).toBe('#00FFDC');
  });

  test('should adjust blue component with positive adjustment', () => {
    // Mocking Math.random to first return a value that selects the blue component, then a value for the adjustment amount
    mockMathRandom(0.66, 0.5); // Cycle 3, adjustment around 35
    expect(generateNewColor('#0000FF')).toBe('#0000D7');
  });

  test('should wrap around for adjustments', () => {
    mockMathRandom(0, 0.9); // Cycle 1, adjustment near 50
    expect(generateNewColor('#FFFFFF')).toMatch(/^#../);
  });
});

import { ApiCollection, ApiData, ApiDocument } from 'interfaces/api';
import { Tag } from 'interfaces/tag';
import { TagService } from 'services/abstract/tag';
import { ElectronDb } from './electron-db';
import { PageCartoStub } from 'interfaces/page-carto';
import { ContentType } from 'services/abstract';

export class LocalTagService extends TagService {
  private db: ElectronDb;

  constructor(db: ElectronDb) {
    super();
    this.db = db;
    this.db.createTable(ContentType.TAGS);
  }

  async getAllTags() {
    const tags = await this.db.getAll<Tag>(ContentType.TAGS);
    const pageCartos = await this.db.getAll<PageCartoStub>(
      ContentType.PAGE_CARTOS
    );
    return {
      ...tags,
      data: tags.data.map((tag) => {
        return {
          ...tag,
          count: pageCartos.data.reduce(
            (acc, curr) => (curr.tags?.includes(tag.id) ? acc + 1 : acc),
            0
          ),
        };
      }),
    };
  }

  async getAllTagsByOwner(_ownerId: number): Promise<ApiCollection<Tag>> {
    return await this.getAllTags();
  }

  private async getTagById(id: number): Promise<ApiData<Tag>> {
    const { data: tag } = await this.db.getById<Tag>(ContentType.TAGS, id);
    return tag;
  }

  async getTagsByPageCarto(
    pageCarto: ApiData<PageCartoStub>
  ): Promise<ApiData<Tag>[]> {
    const tags = (pageCarto.tags || []).map(async (id) => {
      return await this.getTagById(id);
    });
    return await Promise.all(tags);
  }

  async createTag(data: Tag): Promise<ApiDocument<Tag>> {
    return await this.db.insertTableContent(ContentType.TAGS, data, ['name']);
  }
}

import { ipcRenderer } from 'electron';
import db from 'electron-db';
import { ApiCollection, ApiData, ApiDocument } from 'interfaces/api';
import path from 'path';
import { ContentType } from 'services/abstract';

export class ElectronDb {
  public location: string;

  constructor() {
    this.location = path.join(
      ipcRenderer.sendSync('get-path', 'userData'),
      'ElectronDb'
    );
  }

  createTable(tableName: ContentType) {
    return new Promise((resolve, _reject) => {
      db.createTable(tableName, this.location, (success, message) => {
        if (success) {
          resolve(message);
        } else {
          // reject(new Error(message));
        }
      });
    });
  }

  async insertTableContent<T>(
    tableName: ContentType,
    data: T,
    uniqueFields: (keyof T)[] = []
  ): Promise<ApiDocument<T>> {
    const allTags = await this.getAll<T>(tableName);

    const exists = allTags.data.find((item) =>
      uniqueFields.some((key) => item[key] === data[key])
    );
    if (exists) {
      throw new Error('Data already exists!');
    }
    const count = await this.count(tableName);
    const id = count + 1;
    const payload: ApiData<T> = {
      ...data,
      id: id,
      created_at: new Date().toISOString(),
      updated_at: new Date().toISOString(),
    };

    return new Promise((resolve, reject) => {
      db.insertTableContent<T>(
        tableName,
        this.location,
        payload,
        (success, message) => {
          if (success) {
            resolve({
              data: payload,
            });
          } else {
            reject(new Error(message));
          }
        }
      );
    });
  }

  getAll<T>(tableName: ContentType): Promise<ApiCollection<T>> {
    return new Promise((resolve, reject) => {
      db.getAll(tableName, this.location, (success, data) => {
        if (success) {
          resolve({
            data: data,
            meta: { pagination: { total: data.length } },
          });
        } else {
          reject(new Error('Failed to get all data from ' + tableName));
        }
      });
    });
  }

  getRows<T>(tableName: ContentType, conditions: Partial<T>) {
    return new Promise((resolve, reject) => {
      db.getRows(tableName, this.location, conditions, (success, data) => {
        if (success) {
          resolve(data);
        } else {
          reject(new Error('Failed to get data'));
        }
      });
    });
  }

  updateRow<T>(
    tableName: ContentType,
    where: Partial<ApiData<T>>,
    set: Partial<T>
  ) {
    return new Promise((resolve, reject) => {
      db.updateRow(tableName, this.location, where, set, (success, message) => {
        if (success) {
          resolve(message);
        } else {
          reject(new Error(message));
        }
      });
    });
  }

  search<T>(
    tableName: ContentType,
    key: string,
    term: string | number | boolean
  ): Promise<ApiCollection<T>> {
    return new Promise((resolve, reject) => {
      db.search<T>(tableName, this.location, key, term, (success, data) => {
        if (Array.isArray(data)) {
          resolve({
            data: data,
            meta: { pagination: { total: data.length } },
          });
        } else {
          reject(new Error('Search failed'));
        }
      });
    });
  }

  getById<T>(tableName: ContentType, id: number): Promise<ApiDocument<T>> {
    return new Promise((resolve, reject) => {
      db.search<T>(tableName, this.location, 'id', id, (success, data) => {
        if (Array.isArray(data) && data.length > 0) {
          resolve({
            data: data[0],
          });
        } else {
          reject(new Error('Search failed'));
        }
      });
    });
  }

  deleteRow<T>(tableName: ContentType, conditions: Partial<T>) {
    return new Promise((resolve, reject) => {
      db.deleteRow(tableName, this.location, conditions, (success, message) => {
        if (success) {
          resolve(message);
        } else {
          reject(new Error(message));
        }
      });
    });
  }

  getField<T>(tableName: ContentType, key: keyof T) {
    return new Promise((resolve, reject) => {
      db.getField(tableName, this.location, key, (success, data) => {
        if (success) {
          resolve(data);
        } else {
          reject(new Error('Failed to get field data'));
        }
      });
    });
  }

  count(tableName: ContentType): Promise<number> {
    return new Promise((resolve, reject) => {
      db.count(tableName, this.location, (success, count) => {
        if (success) {
          resolve(count);
        } else {
          reject(new Error('Failed to get count'));
        }
      });
    });
  }
}

export const electronDb = new ElectronDb();

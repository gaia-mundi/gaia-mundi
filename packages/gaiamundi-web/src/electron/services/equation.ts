import { Equation } from 'interfaces/equation';
import { EquationService } from 'services/abstract/equation';
import { ElectronDb } from './electron-db';
import { ApiCollection } from 'interfaces/api';
import { ContentType } from 'services/abstract';

export class LocalEquationService extends EquationService {
  private db: ElectronDb;

  constructor(db: ElectronDb) {
    super();
    this.db = db;

    this.db.createTable(ContentType.EQUATION).then(() => {
      this.db.insertTableContent<Equation>(ContentType.EQUATION, {
        label: 'Valeur simple : A',
        value: 'A',
      });
      this.db.insertTableContent<Equation>(ContentType.EQUATION, {
        label: 'Somme : A + B',
        value: 'A + B',
      });
    });
  }

  async getEquations(): Promise<ApiCollection<Equation>> {
    return await this.db.getAll<Equation>(ContentType.EQUATION);
  }
}

import fs from 'fs';
import path from 'path';
import { ApiCollection, ApiData, ApiDocument } from 'interfaces/api';
import { UploadedFile } from 'interfaces/file';
import { PageCarto, PageCartoStub } from 'interfaces/page-carto';
import { User } from 'interfaces/user';
import { PageCartoService } from 'services/abstract/page-carto';
import { readFile } from 'utils/file';
import { LocalDatasetService } from './dataset';
import { ElectronDb } from './electron-db';
import { LocalGeoMapService } from './geo-map';
import { LocalIndicatorService } from './indicator';
import { LocalTagService } from './tag';
import { LOCAL_USER } from './user.ts';
import { ContentType } from 'services/abstract';
import { DESKTOP_IMAGES } from '../renderer/components/Assets';

export class LocalPageCartoService extends PageCartoService {
  private db: ElectronDb;
  private tagService: LocalTagService;
  private geoMapService: LocalGeoMapService;
  private datasetService: LocalDatasetService;
  private indicatorService: LocalIndicatorService;
  private pageCartoDir: string;

  constructor(db: ElectronDb) {
    super();
    this.db = db;
    this.tagService = new LocalTagService(db);
    this.datasetService = new LocalDatasetService(db);
    this.geoMapService = new LocalGeoMapService(db);
    this.indicatorService = new LocalIndicatorService(db);

    this.db.createTable(ContentType.PAGE_CARTOS);

    this.pageCartoDir = path.join(this.db.location, 'pagecarto');
    if (!fs.existsSync(this.pageCartoDir)) {
      fs.mkdirSync(this.pageCartoDir, { recursive: true });
    }
  }

  async createPageCarto(
    data: PageCartoStub
  ): Promise<ApiDocument<PageCartoStub>> {
    return await this.db.insertTableContent(ContentType.PAGE_CARTOS, {
      ...data,
      data_fragments: [],
      html: '',
    });
  }

  async getLatestPageCartos(
    _page: number,
    _pageSize: number
  ): Promise<ApiCollection<PageCarto>> {
    return await this.getPageCartoByTagsAndSearch(_page, '', [], LOCAL_USER);
  }

  async getPageCartoById(id: number): Promise<ApiDocument<PageCarto>> {
    const pageCarto = await this.db.getById<PageCartoStub>(
      ContentType.PAGE_CARTOS,
      id
    );
    const tags = await this.tagService.getTagsByPageCarto(pageCarto.data);
    const geoMap = await this.geoMapService.getGeoMapById(
      pageCarto.data.map as number
    );
    const dataFragments =
      await this.datasetService.getDataFragmentsByPageCartoId(
        pageCarto.data.id
      );
    const indicators = await this.indicatorService.getIndicatorsByPageCartoId(
      pageCarto.data.id
    );

    const { data: cover } = pageCarto.data.cover
      ? await this.db.getById<UploadedFile>(
          ContentType.FILES,
          pageCarto.data.cover as number
        )
      : { data: undefined };

    return {
      ...pageCarto,
      data: {
        ...pageCarto.data,
        tags: tags,
        map: geoMap,
        owner: LOCAL_USER,
        data_fragments: dataFragments,
        cover: cover,
        indicators: indicators,
      },
    };
  }

  async uploadCsv(file: File): Promise<UploadedFile> {
    const csv = await readFile(file);
    const { data: uploadedFile } =
      await this.db.insertTableContent<UploadedFile>(ContentType.FILES, {
        name: file.name,
        formats: {},
        hash: '',
        ext: 'csv',
        mime: file.type,
        size: file.size,
        url: '',
        provider: '',
      } as UploadedFile);

    const filePath = path.join(
      this.db.location,
      'csv',
      `${uploadedFile.id}.csv`
    );

    await fs.promises.writeFile(filePath, csv);

    return uploadedFile;
  }

  async getPageCartoByTagsAndSearch(
    _page: number,
    searchKeywords: string,
    selectedTags: number[],
    _user: User | undefined
  ): Promise<ApiCollection<PageCarto>> {
    const results = await this.db.search<PageCartoStub>(
      ContentType.PAGE_CARTOS,
      'name',
      searchKeywords
    );
    const pageCartos = await Promise.all(
      results.data
        // Filter by tag
        .filter(
          (pageCarto) =>
            selectedTags.length === 0 ||
            selectedTags.some((tag) => pageCarto.tags?.includes(tag))
        )
        // Populate each
        .map(({ id }) => this.getPageCartoById(id).then(({ data }) => data))
    );

    return {
      ...results,
      data: pageCartos,
    };
  }

  async updatePageCarto(
    id: number,
    data: Partial<PageCartoStub>
  ): Promise<ApiDocument<PageCarto>> {
    await this.db.updateRow<PageCartoStub>(
      ContentType.PAGE_CARTOS,
      { id: id },
      data
    );
    return await this.getPageCartoById(id);
  }

  async uploadCover(file: File, pageCartoId: number): Promise<UploadedFile> {
    const { data: uploadedFile } =
      await this.db.insertTableContent<UploadedFile>(ContentType.FILES, {
        name: file.name || pageCartoId,
        formats: {},
        hash: '',
        ext: 'png',
        mime: file.type,
        size: file.size,
        url: '',
        provider: '',
      } as UploadedFile);
    const filePath = path.join(
      this.pageCartoDir,
      `${uploadedFile.id.toString()}.png`
    );
    const buffer = Buffer.from(await file.arrayBuffer());
    await fs.promises.writeFile(filePath, buffer);

    await this.updatePageCarto(pageCartoId, {
      cover: uploadedFile.id,
    });

    return uploadedFile;
  }

  async deletePageCarto(id: number): Promise<void> {
    await this.db.deleteRow(ContentType.PAGE_CARTOS, { id: id });
  }

  getImageUrl = (
    cover: ApiData<UploadedFile> | undefined,
    _format: 'thumbnail' | 'small' | 'medium' | 'large' = 'thumbnail'
  ) => {
    if (cover) {
      const coverPath = path.join(this.pageCartoDir, `${cover.id}.png`);
      return fs.existsSync(coverPath)
        ? `local://${coverPath}`
        : DESKTOP_IMAGES.MAP_PLACEHOLDER_THUMBNAIL;
    }

    return DESKTOP_IMAGES.MAP_PLACEHOLDER_THUMBNAIL;
  };
}

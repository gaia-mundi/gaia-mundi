import { ApiData, ApiDocument } from 'interfaces/api';
import { IndicatorStub } from 'interfaces/indicator';
import { IndicatorService } from 'services/abstract/indicator';
import { ElectronDb } from './electron-db';
import { ContentType } from 'services/abstract';

export class LocalIndicatorService extends IndicatorService {
  private db: ElectronDb;

  constructor(db: ElectronDb) {
    super();
    this.db = db;

    this.db.createTable(ContentType.INDICATOR);
  }

  async getIndicatorsByPageCartoId(
    pageCartoId: number
  ): Promise<ApiData<IndicatorStub>[]> {
    const { data: indicators } = await this.db.search<IndicatorStub>(
      ContentType.INDICATOR,
      'page_carto',
      pageCartoId
    );
    return indicators;
  }

  async addIndicatorToPageCarto(
    pageCartoId: number,
    indicator: IndicatorStub
  ): Promise<ApiDocument<IndicatorStub>> {
    return await this.db.insertTableContent<IndicatorStub>(
      ContentType.INDICATOR,
      {
        ...indicator,
        page_carto: pageCartoId,
      }
    );
  }
}

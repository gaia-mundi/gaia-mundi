import { ContentType, DatastoreService } from 'services/abstract';
import { LocalChartService } from './chart';
import { LocalDatasetService } from './dataset';
import { electronDb } from './electron-db';
import { LocalEquationService } from './equation';
import { LocalGeoMapService } from './geo-map';
import { LocalIndicatorService } from './indicator';
import { LocalPageCartoService } from './page-carto';
import { LocalSnapshotService } from './snapshot';
import { LocalTagService } from './tag';
import { LocalUserService } from './user.ts';

const SERVICES: DatastoreService = {
  [ContentType.CHARTS]: new LocalChartService(electronDb),
  [ContentType.DATASET]: new LocalDatasetService(electronDb),
  [ContentType.DATA_FRAGMENT]: new LocalDatasetService(electronDb),
  [ContentType.EQUATION]: new LocalEquationService(electronDb),
  [ContentType.GEO_MAPS]: new LocalGeoMapService(electronDb),
  [ContentType.INDICATOR]: new LocalIndicatorService(electronDb),
  [ContentType.PAGE_CARTOS]: new LocalPageCartoService(electronDb),
  [ContentType.SNAPSHOTS]: new LocalSnapshotService(electronDb),
  [ContentType.TAGS]: new LocalTagService(electronDb),
  [ContentType.USERS]: new LocalUserService(),
};

export default SERVICES;

import fs from 'fs';
import { GeoJSON2SVG } from 'geojson2svg';
import { ApiCollection, ApiData, ApiDocument } from 'interfaces/api';
import { UploadedFile } from 'interfaces/file';
import { GeoMap, GeoMapStub } from 'interfaces/geo-map';
import path from 'path';
import { GeoMapService } from 'services/abstract/geo-map';
import { readFile, validateGeoJsonFile } from 'utils/file';
import { ElectronDb } from './electron-db';
import { LOCAL_USER } from './user.ts';
import { GeoJSON } from 'interfaces/geojson';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import parsePath from 'parse-svg-path';
import { ContentType } from 'services/abstract';

export class LocalGeoMapService extends GeoMapService {
  private db: ElectronDb;
  private geoJsonDir: string;

  constructor(db: ElectronDb) {
    super();
    this.db = db;

    this.db.createTable(ContentType.GEO_MAPS);
    this.db.createTable(ContentType.FILES);

    this.geoJsonDir = path.join(this.db.location, 'geojson');
    const thumbnailsDir = path.join(this.geoJsonDir, 'thumbnails');
    if (!fs.existsSync(thumbnailsDir)) {
      fs.mkdirSync(thumbnailsDir, { recursive: true });
    }
  }

  async getGeoMaps(
    _page: number,
    _pageSize: number,
    _userId?: number
  ): Promise<ApiCollection<GeoMap>> {
    const geoJsons = await this.db.getAll<UploadedFile>(ContentType.FILES);
    const geoMaps = await this.db.getAll<GeoMapStub>(ContentType.GEO_MAPS);
    return {
      ...geoMaps,
      data: geoMaps.data.map((geoMap) => {
        return {
          ...geoMap,
          owner: LOCAL_USER,
          geoJSON: geoJsons.data.find(
            ({ id }) => geoMap.geoJSON === id
          ) as ApiData<UploadedFile>,
        };
      }),
    };
  }

  async createGeoMap(data: GeoMapStub): Promise<ApiDocument<GeoMapStub>> {
    return await this.db.insertTableContent(ContentType.GEO_MAPS, data);
  }

  async generateThumbnail(data: string, id: number) {
    const converterSVG = new GeoJSON2SVG({
      attributes: {
        style: 'stroke:black; fill: #FFFFFF;stroke-width:0.5px',
      },
    });

    const converterPath = new GeoJSON2SVG({
      output: 'path',
    });

    const geoJson = JSON.parse(data);
    const dataSVG = converterSVG.convert(geoJson);
    const dataPaths = converterPath.convert(geoJson);

    let width = 0;
    let height = 0;

    for (const path of dataPaths) {
      const points = parsePath(path);
      points.pop();
      const [maxX, maxY] = points.reduce(
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        ([xMax, yMax], [, xCurrent, yCurrent]) => [
          Math.max(xMax, xCurrent),
          Math.max(yMax, yCurrent),
        ],
        [0, 0]
      );
      if (maxY > height) height = maxY;
      if (maxX > width) width = maxX;
    }
    const dataSVGElements = dataSVG.join('\n');
    const svg = `<?xml version="1.0" standalone="no"?>\n<svg xmlns="http://www.w3.org/2000/svg" width="${width}" height="${height}">\n${dataSVGElements}\n</svg>`;
    const svgPath = path.join(this.geoJsonDir, 'thumbnails', `${id}.svg`);
    await fs.promises.writeFile(svgPath, svg);
  }

  async uploadGeoJson(file: File): Promise<UploadedFile> {
    const fileToUpload = await this.createGeoJson(file);
    const json = await readFile(fileToUpload);
    const { data: uploadedFile } =
      await this.db.insertTableContent<UploadedFile>(ContentType.FILES, {
        name: file.name,
        formats: {},
        hash: '',
        ext: 'json',
        mime: file.type,
        size: file.size,
        url: '',
        provider: '',
      } as UploadedFile);

    const filePath = path.join(this.geoJsonDir, `${uploadedFile.id}.json`);

    await fs.promises.writeFile(filePath, json);

    await this.generateThumbnail(json, uploadedFile.id);

    return uploadedFile;
  }

  async getGeoJson(geoJsonFile: ApiData<UploadedFile>) {
    const filePath = path.join(this.geoJsonDir, `${geoJsonFile.id}.json`);
    if (fs.existsSync(filePath)) {
      const json = fs.readFileSync(filePath, 'utf8');
      return JSON.parse(json) as GeoJSON;
    } else {
      throw new Error('File does not exist!');
    }
  }

  getGeoMapThumbnailUrlById(id: number): string {
    const filePath = path.join(this.geoJsonDir, 'thumbnails', `${id}.svg`);
    return `local://${filePath}`;
  }

  private async createGeoJson(file: File): Promise<File> {
    if (file.type === 'application/json') {
      return file;
    }
    const GeoJsonFileContent = await validateGeoJsonFile(file);
    return new File([JSON.stringify(GeoJsonFileContent)], `${file.name}.json`, {
      type: 'application/json',
    });
  }

  async getGeoMapById(id: number): Promise<ApiData<GeoMap>> {
    const geoMap = await this.db.getById<GeoMapStub>(ContentType.GEO_MAPS, id);
    const geoJson = await this.getGeoJsonById(geoMap.data.geoJSON as number);
    return {
      ...geoMap.data,
      geoJSON: geoJson.data,
      owner: LOCAL_USER,
    };
  }

  private async getGeoJsonById(id: number) {
    return await this.db.getById<UploadedFile>(ContentType.FILES, id);
  }
}

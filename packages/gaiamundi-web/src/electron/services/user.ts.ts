import { ApiData } from 'interfaces/api';
import { UploadedFile } from 'interfaces/file';
import { User, UserAuthResponse, UserSignUpFields } from 'interfaces/user';
import { UserService } from 'services/abstract/user';

export const LOCAL_USER: User = {
  id: 1,
  email: 'user@local.com',
  username: 'Local User',
  password: '',
  provider: 'local',
  confirmed: true,
  blocked: false,
  created_at: new Date().toISOString(),
  updated_at: new Date().toISOString(),
};

export class LocalUserService extends UserService {
  private readonly user: User = LOCAL_USER;

  constructor() {
    super();
  }

  async getCurrentUser(): Promise<User> {
    return Promise.resolve(this.user);
  }

  async updateCurrentUser(
    _userId: number,
    _data: UserSignUpFields
  ): Promise<User> {
    throw new Error('Not implemented!');
  }

  async sendForgotPasswordEmail(_email: string): Promise<boolean> {
    throw new Error('Not implemented!');
  }

  async login(_email: string, _password: string): Promise<UserAuthResponse> {
    throw new Error('Not implemented!');
  }

  async resetPassword(
    _code: string,
    _password: string,
    _password2: string
  ): Promise<UserAuthResponse> {
    throw new Error('Not implemented!');
  }

  async signUp(_newUser: UserSignUpFields): Promise<UserAuthResponse> {
    throw new Error('Not implemented!');
  }

  async uploadAvatar(_file: File, _refId: number): Promise<UploadedFile> {
    throw new Error('Not implemented!');
  }

  getAvatarUrl = (
    _cover: ApiData<UploadedFile>,
    _format: 'thumbnail' | 'small' | 'medium' | 'large' = 'thumbnail'
  ) => {
    throw new Error('Not implemented!');
  };

  async logout() {
    throw new Error('Not implemented!');
  }
}

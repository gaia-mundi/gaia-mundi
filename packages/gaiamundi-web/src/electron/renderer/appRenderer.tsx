import React from 'react';
import { createRoot } from 'react-dom/client';
import Application from './components/Application';
import '../../index.css';

// Render application in DOM
createRoot(document.getElementById('app') as HTMLElement).render(
  <Application />
);

import { Header } from 'components/Layout/Header';
import { Dashboard } from 'components/PageCarto/Dashboard/Dashboard';

export const Home: React.FC = () => {
  return (
    <div className="w-full">
      <Header>Mes PageCartos</Header>
      <Dashboard />
    </div>
  );
};

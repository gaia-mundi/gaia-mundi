import { AuthProvider } from 'hooks/useAuth';
import { ToastProvider } from 'hooks/useToast';
import { NewPageCartoPage } from 'pages/NewPageCartoPage/NewPageCartoPage';
import { PageCartoEditPage } from 'pages/PageCartoEditPage/PageCartoEditPage';
import { PageCartoViewPage } from 'pages/PageCartoViewPage/PageCartoViewPage';
import React from 'react';
import { QueryClient, QueryClientProvider } from 'react-query';
import { HashRouter, Route, Routes } from 'react-router-dom';
import { Home } from './Home';
import { Layout } from './Layout';

import 'react-data-grid/lib/styles.css';
import './index.scss';

// Create a client
const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false, // default: true
    },
  },
});

const Application: React.FC = () => {
  return (
    <QueryClientProvider client={queryClient}>
      <ToastProvider>
        <HashRouter>
          <AuthProvider>
            <Routes>
              <Route path="/" element={<Layout />}>
                <Route index element={<Home />} />
                <Route
                  path="page-carto/create"
                  element={<NewPageCartoPage />}
                />
                <Route
                  path="page-carto/:id/edit"
                  element={<PageCartoEditPage />}
                />
                <Route path="page-carto/:id" element={<PageCartoViewPage />} />
                <Route
                  path="page-carto/:id/:snapshot"
                  element={<PageCartoViewPage />}
                />
                <Route
                  path="page-carto/:id/:snapshot/:geocode"
                  element={<PageCartoViewPage />}
                />
              </Route>
            </Routes>
          </AuthProvider>
        </HashRouter>
      </ToastProvider>
    </QueryClientProvider>
  );
};

export default Application;

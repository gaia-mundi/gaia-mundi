import { Alert } from 'components/Alert/Alert';
import { ApiErrorAlert } from 'components/Alert/ApiErrorMessage';
import { LoadingMessage } from 'components/Loader/LoadingMessage';
import PageCartoItem from 'components/PageCarto/List/PageCartoItem';
import { Pagination } from 'components/Pagination/Pagination';
import { useService } from 'hooks/useService';
import { ApiError } from 'interfaces/api';
import { useState } from 'react';
import { useQuery } from 'react-query';
import { ContentType } from 'services/abstract';

export const PageCartoList = () => {
  const [page, setPage] = useState(1);
  const paginationLimit = 9;
  const pageCartoService = useService(ContentType.PAGE_CARTOS);
  const {
    data: response,
    isError,
    error,
    isLoading,
  } = useQuery({
    queryKey: ['latest-page-carto', page],
    queryFn: async () => {
      return await pageCartoService.getLatestPageCartos(page, paginationLimit);
    },
    keepPreviousData: true,
  });

  if (isLoading) {
    return <LoadingMessage data-testid="loading-message" />;
  }

  if (isError) {
    return <ApiErrorAlert error={error as ApiError} />;
  }

  if (!response || response.data.length === 0) {
    return <Alert type="info">Aucun contenu à afficher.</Alert>;
  }

  const { data, meta } = response;

  return (
    <div>
      <div className="grid grid-cols-3 gap-y-10 gap-x-6">
        {data.map((page) => {
          return <PageCartoItem key={page.id} {...page} />;
        })}
      </div>
      <div className="flex flex-row mt-5 justify-center">
        <Pagination
          page={page}
          onPaginate={(p: number) => setPage(p)}
          totalPages={meta.pagination.pageCount || 0}
        />
      </div>
    </div>
  );
};

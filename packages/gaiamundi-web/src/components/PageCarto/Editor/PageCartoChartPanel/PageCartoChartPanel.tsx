import { PencilIcon, PlusIcon, TrashIcon } from '@heroicons/react/24/solid';
import { Button } from 'components/Button/Button';
import ButtonGroup from 'components/Button/ButtonGroup';
import { Chart } from 'components/ChartEngine/Chart/Chart';
import { ChartEngine } from 'components/ChartEngine/ChartEngine';
import ChartPlaceholder from 'components/Icons/Charts/ChartPlaceholder';
import { ListBoxInput } from 'components/Inputs/ListBoxInput';
import Well from 'components/Layout/Well';
import { LoadingMessage } from 'components/Loader/LoadingMessage';
import { useCanEdit } from 'hooks/useCanEdit';
import { useData } from 'hooks/useData';
import { useConfirmModal, useModal } from 'hooks/useModal';
import { usePageCarto } from 'hooks/usePageCarto';
import { useService } from 'hooks/useService';
import { useToast } from 'hooks/useToast';
import { useMemo, useState } from 'react';
import { useMutation, useQuery, useQueryClient } from 'react-query';
import { ContentType } from 'services/abstract';

export const PageCartoChartPanel = ({ readOnly = false }) => {
  const { showModal, hideModal } = useModal();
  const { selectedGeoCode } = useData();
  const { pageCartoId } = usePageCarto();
  const canEdit = useCanEdit();
  const chartService = useService(ContentType.CHARTS);
  const { data: response, isLoading: isFetching } = useQuery({
    queryKey: ['page-carto-charts', pageCartoId],
    queryFn: async () => {
      return await chartService.getChartsByCartoPage(pageCartoId);
    },
    onSuccess({ data }) {
      setSelectedChartId(data.length ? data[data.length - 1].id : 0);
    },
  });
  const charts = useMemo(() => response?.data || [], [response]);
  const [selectedChartId, setSelectedChartId] = useState(
    charts.length ? charts[0].id : 0
  );
  const { addToast } = useToast();
  const queryClient = useQueryClient();

  const chartOptions = useMemo(() => {
    return charts.map(({ id, name }) => ({
      value: id,
      label: name,
    })) as Array<{ value: number; label: string }>;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [charts]);

  const { mutateAsync: discardChart, isLoading: isDeleting } = useMutation({
    mutationFn: async () => {
      return await chartService.deleteChart(selectedChartId);
    },
    onSuccess: () => {
      addToast({
        title: `Mise à jour`,
        type: 'success',
        description: `Le graphique a été supprimé avec succès`,
      });
      queryClient.invalidateQueries({
        queryKey: ['page-carto-charts', pageCartoId],
      });
    },
    onError: (error) => {
      addToast({
        title: 'Echec lors de la suppression du graphique',
        type: 'error',
        description: JSON.stringify(error),
      });
    },
  });

  const { showConfirmModal } = useConfirmModal(
    'Supprimer graphique',
    'Êtes-vous sûr de vouloir supprimer cette graphique?',
    discardChart
  );

  if (isFetching || isDeleting) {
    return <LoadingMessage />;
  }

  if (readOnly && response?.data.length === 0) {
    return null;
  }

  return (
    <div className="w-full h-full">
      {selectedGeoCode && charts.length > 0 ? (
        <Well title="Graphiques">
          <div className="flex items-center absolute top-0 right-0 m-4">
            <ListBoxInput<number>
              defaultValue={selectedChartId}
              options={chartOptions}
              onChange={(chartId: number) => {
                setSelectedChartId(chartId);
              }}
            />
            {canEdit && (
              <ButtonGroup className="ml-2">
                <Button
                  icon={PlusIcon}
                  data-testid="create-chart2"
                  onClick={() =>
                    showModal({
                      title: 'Nouveau graphique',
                      Component: ChartEngine,
                      props: {
                        chartId: 0,
                        pageCartoId,
                        onSubmit: hideModal,
                        selectedGeoCode,
                      },
                    })
                  }
                  size={'sm'}
                />
                <Button
                  icon={PencilIcon}
                  data-testid="edit-chart"
                  onClick={() =>
                    showModal({
                      title: 'Modifier graphique',
                      Component: ChartEngine,
                      props: {
                        chartId: selectedChartId,
                        pageCartoId,
                        onSubmit: hideModal,
                        selectedGeoCode,
                      },
                    })
                  }
                  size={'sm'}
                />
                <Button
                  icon={TrashIcon}
                  data-testid="discard-chart"
                  onClick={showConfirmModal}
                  size={'sm'}
                />
              </ButtonGroup>
            )}
          </div>
          <Chart chartId={selectedChartId} />
        </Well>
      ) : !readOnly ? (
        <Well title="Graphiques">
          <div className="flex justify-center h-[256px] m-4 overflow-hidden">
            <ChartPlaceholder className="absolute z-0 w-2/4 h-2/3" />
            <Button
              icon={PlusIcon}
              data-testid="create-chart"
              onClick={() =>
                showModal({
                  title: 'Nouveau graphique',
                  Component: ChartEngine,
                  props: {
                    pageCartoId,
                    onSubmit: hideModal,
                    selectedGeoCode,
                  },
                })
              }
              className="absolute m-auto left-0 right-0 top-1/2 z-1"
              disabled={!selectedGeoCode}
            >
              {selectedGeoCode
                ? `Nouveau graphique`
                : `Veuillez sélectionner une maille`}
            </Button>
          </div>
        </Well>
      ) : null}
    </div>
  );
};

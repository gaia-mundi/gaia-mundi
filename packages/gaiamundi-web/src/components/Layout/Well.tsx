import * as React from 'react';

interface WellProps {
  title?: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  Icon?: React.FC<any>;
  children: React.ReactNode;
}

const Well = ({ title, Icon, children }: WellProps): JSX.Element => {
  return (
    <div
      className="relative rounded-sm border border-gray-200 bg-white shadow-md p-2"
      data-testid="well-component"
    >
      {title && (
        <h3 className="mb-2 text-sm font-medium text-gray-900 title-font">
          {Icon && <Icon className="h-5 w-5 text-blue-500 inline" />} {title}
        </h3>
      )}
      <div className="shadow-none text-xs">{children}</div>
    </div>
  );
};

export default Well;

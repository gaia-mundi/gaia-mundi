import classNames from 'classnames';
import type { ComponentProps, FC, PropsWithChildren } from 'react';

type Color = 'gray' | 'info' | 'failure' | 'warning' | 'success';

export interface HelperTextProps
  extends PropsWithChildren<Omit<ComponentProps<'p'>, 'color'>> {
  color?: Color;
  value?: string;
}

const HELPER_TEXT_COLORS: Record<Color, string> = {
  gray: 'text-gray-500',
  info: 'text-blue-700',
  success: 'text-green-600',
  failure: 'text-red-600',
  warning: 'text-yellow-500',
};

export const HelperText: FC<HelperTextProps> = ({
  children,
  className,
  color = 'info',
  value,
  ...props
}) => {
  return (
    <p
      className={classNames(
        'mt-2 text-sm',
        HELPER_TEXT_COLORS[color],
        className
      )}
      {...props}
    >
      {value ?? children ?? ''}
    </p>
  );
};

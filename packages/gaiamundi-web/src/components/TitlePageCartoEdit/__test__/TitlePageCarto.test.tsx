import '@testing-library/jest-dom/extend-expect';
import { fireEvent, render, waitFor } from '@testing-library/react';
import { QueryClient, QueryClientProvider } from 'react-query';
import { TitlePageCartoEdit } from '../TitlePageCartoEdit';

const mockUpdatePageCarto = jest.fn().mockResolvedValue({
  data: {
    id: 1,
    name: 'Titre modifié',
  },
});

const mockPageCartoData = {
  id: 1,
  name: 'Titre initial',
};

jest.mock('hooks/useService', () => {
  return {
    useService() {
      return {
        updatePageCarto: mockUpdatePageCarto,
      };
    },
  };
});

jest.mock('hooks/usePageCarto', () => {
  return {
    usePageCarto() {
      return {
        data: {
          data: mockPageCartoData,
        },
      };
    },
  };
});

describe('TitlePageCartoEdit', () => {
  it('renders the initial title', async () => {
    const queryClient = new QueryClient();
    const { getByTestId } = render(
      <QueryClientProvider client={queryClient}>
        <TitlePageCartoEdit />
      </QueryClientProvider>
    );

    await waitFor(() => {
      expect(getByTestId('content-editable').innerText).toEqual(
        mockPageCartoData.name
      );
    });
  });

  it('updates the title when edited and calls updatePageCarto with the correct parameters', async () => {
    const queryClient = new QueryClient();
    const { getByTestId } = render(
      <QueryClientProvider client={queryClient}>
        <TitlePageCartoEdit />
      </QueryClientProvider>
    );

    await waitFor(() => {
      expect(getByTestId('content-editable')).toBeInTheDocument();
    });

    const inputElement = getByTestId('content-editable');
    const newTitle = 'Titre modifié';
    fireEvent.input(inputElement, { target: { innerText: newTitle } });
    fireEvent.blur(inputElement);

    await waitFor(() => {
      expect(inputElement.innerText).toEqual(newTitle);
      expect(mockUpdatePageCarto).toHaveBeenCalledWith(mockPageCartoData.id, {
        name: newTitle,
      });
    });
  });

  it('calls handleBlur when the input element loses focus', async () => {
    const queryClient = new QueryClient();
    const { getByTestId } = render(
      <QueryClientProvider client={queryClient}>
        <TitlePageCartoEdit />
      </QueryClientProvider>
    );

    await waitFor(() => {
      expect(getByTestId('content-editable')).toBeInTheDocument();
    });

    const inputElement = getByTestId('content-editable');

    const differentTitle = 'Titre différent';

    fireEvent.input(inputElement, {
      target: { innerText: differentTitle },
    });

    fireEvent.blur(inputElement);

    await waitFor(() => {
      expect(mockUpdatePageCarto).toHaveBeenCalledWith(mockPageCartoData.id, {
        name: differentTitle,
      });
    });
  });
});

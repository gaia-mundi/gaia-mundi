module.exports = {
  afterFindMany: async ({ result, params }) => {
    result.forEach((tag, idx) => {
      if (tag.page_cartos) {
        const ownerId = params.where?.page_cartos?.owner?.id?.$eq;
        const pageCartos = tag.page_cartos;
        const count = ownerId
          ? pageCartos.filter((p) => p.owner.id == ownerId).length
          : pageCartos.length;
        delete tag.page_cartos;
        result[idx].count = count;
      }
    });
  },
};
